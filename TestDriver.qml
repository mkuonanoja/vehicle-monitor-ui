import QtQuick 2.4

Item {
    id: root

    property bool running: false
    property Item vehicleItem

    Timer {
        id: timer

        repeat: true
        running: root.running
        interval: 2000

        onTriggered: {
            updateVehicle();
        }
    }

    function updateVehicle() {
        if (root.vehicleItem) {
            root.vehicleItem.currentSpeed = randomValue(60,120);
            root.vehicleItem.travelledLength += 0.01;
        }
    }

    function randomValue(min, max) {
        return Math.random() * (max - min + 1) + min;
    }
}
