import QtQuick 2.0

Rectangle {
    id: root

    property double currentSpeed: 0.0
    property double travelledLenth: 0.0
    property double speedLimit: 100.0

    gradient: Gradient {
        GradientStop { position: 0.00; color: "#004AB6" }
        GradientStop { position: 0.15; color: "#002761" }
        GradientStop { position: 0.50; color: "#000000" }
        GradientStop { position: 0.85; color: "#002761" }
        GradientStop { position: 1.00; color: "#004AB6" }
    }

    Item {
        anchors.left: parent.left
        anchors.right: speedMonitor.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        SpeedLimitMonitor {
            id: speedLimitMonitor

            speedLimit: root.speedLimit

            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 30
        }
    }

    SpeedMonitor {
        id: speedMonitor

        speed: root.currentSpeed
        speedLimit :root.speedLimit

        anchors.centerIn: parent
    }

    Item {
        anchors.right: parent.right
        anchors.left: speedMonitor.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        TravelMonitor {
            id: travelMonitor

            travelCounterValue: root.travelledLenth

            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 30
        }
    }
}
