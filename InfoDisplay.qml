import QtQuick 2.0

Rectangle {
    id: root
    width: 100
    height: 62

    gradient: Gradient {
        GradientStop { position: 0.0; color: "#070F1A" }
        GradientStop { position: 0.5; color: "#002459" }
        GradientStop { position: 1.0; color: "#070F1A" }
    }
}
