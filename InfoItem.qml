import QtQuick 2.0

Item {
    id: root
    property string label
    property string value
    property string units

    width: childrenRect.width
    height: childrenRect.height

    Text {
        id: labelText

        text: root.label
        font.pointSize: 14
        color: "#4791FF"
        anchors.left: parent.left
    }

    Text {
        id: valueText

        text: root.value
        font.pointSize: 20
        color: "#AFCFFF"
        anchors.left: labelText.right
        anchors.leftMargin: 8
        anchors.baseline: labelText.baseline
    }

    Text {
        id: unitsText

        text: root.units
        font.pointSize: 14
        color: "#4791FF"
        anchors.left: valueText.right
        anchors.leftMargin: 8
        anchors.baseline: labelText.baseline
    }
}
