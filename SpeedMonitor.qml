import QtQuick 2.0

Item {
    id: root

    property double speed: 0
    property double speedLimit: 100.0

    width: 120
    height: speedText.height + barMeter.height

    Text {
        id: speedText

        text: speed.toFixed(0)
        font.pointSize: 64
        color: "#AFCFFF"
        anchors.right: parent.right
    }

    Text {
        id: unitsText

        text: "km/h"
        font.pointSize: 26
        color: "#4791FF"
        anchors.left: speedText.right
        anchors.leftMargin: 10
        anchors.baseline: speedText.baseline
    }

    BarMeter {
        id: barMeter

        ticks: maxTicks * root.speed / root.speedLimit;
        maxTicks: 16
        maxOverflowTics: 10

        anchors.left: parent.left
        anchors.right: parent.right // speedText.right
        anchors.top: speedText.bottom
    }
}
