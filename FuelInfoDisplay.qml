import QtQuick 2.0

InfoDisplay {
    id: root

    property double averageFuelConsumption: 0
    property double totalFuelConsumption: 0
    property double totalFuelCosts: 0

    Item {
        anchors.left: parent.left
        anchors.right: totalFuelConsumptionInfo.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        InfoItem {
            id: averageFuelConsumptionInfo

            label: ""
            value: root.averageFuelConsumption
            units: "l/100 km"

            anchors.centerIn: parent
        }
    }

    InfoItem {
        id: totalFuelConsumptionInfo

        label: ""
        value: root.totalFuelConsumption
        units: "l/100 km"

        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Item {
        anchors.left: totalFuelConsumptionInfo.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        InfoItem {
            id: totalFuelCostsInfo

            label: ""
            value: root.totalFuelCosts.toFixed(1)
            units: "€"

            anchors.centerIn: parent
        }
    }
}
