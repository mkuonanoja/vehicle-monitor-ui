import QtQuick 2.4
import QtQuick.Controls 1.3

ApplicationWindow {
    title: qsTr("Vehicle Monitor")
    width: 640
    height: 400

    RouteInfoDisplay {
        id: topDisplay

        routeLeft: 23.32131
        timeLeft: 17.3232

        height: parent.height * 0.17
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
    }

    MainDisplay {
        id: centerDisplay

        speedLimit : 80.00
        currentSpeed : vehicle.currentSpeed
        travelledLenth: vehicle.travelledLength

        anchors.top: topDisplay.bottom
        anchors.bottom: bottomDisplay.top
        anchors.left: parent.left
        anchors.right: parent.right
    }

    FuelInfoDisplay {
        id: bottomDisplay

        averageFuelConsumption: 7.2
        totalFuelConsumption: 2.3
        totalFuelCosts: 3.1032

        height: parent.height * 0.17
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }

    TestDriver {
        id: testDriver

        vehicleItem: Item {
            id: vehicle

            property int currentSpeed: 0
            property double travelledLength: 0

            Behavior on currentSpeed {
                NumberAnimation {
                    id: speedAnimation
                    duration: 6000
                }
            }
        }
    }

    Component.onCompleted: {
        testDriver.running = true;
    }
}
