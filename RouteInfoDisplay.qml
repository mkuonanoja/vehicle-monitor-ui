import QtQuick 2.0

InfoDisplay {
    id: root

    property double routeLeft
    property double timeLeft

    Item {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: parent.width * 0.5

        InfoItem {
            id: routeLeftInfo

            label: "Route left"
            value: root.routeLeft.toFixed(1)
            units: "km"

            anchors.centerIn: parent
        }
    }

    Item {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: parent.width * 0.5

        InfoItem {
            id: timeLeftInfo

            label: "Time left"
            value: root.timeLeft.toFixed(1)
            units: "min"

            anchors.centerIn: parent
        }
    }
}
