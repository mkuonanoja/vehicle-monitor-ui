import QtQuick 2.0

Item {
    id: root

    property int ticks
    property int maxTicks
    property int maxOverflowTics

    height: childrenRect.height

    Row {
        id: tickRow
        spacing: tickSpacing
        property int tickWidth
        property int tickHeight
        layoutDirection: Qt.RightToLeft
        anchors.right: parent.right
        anchors.top: parent.top
        Repeater {
            id: tickRepeater
            model: root.ticks
            delegate: Rectangle {width: tickRow.tickWidth; height: tickRow.tickHeight; color: if (index < root.maxTicks) {"#4791FF"} else {"#ff0000"}}
        }
    }

    Rectangle {
        id: maxTicksBar
        color: "#4791FF"
        anchors.top: tickRow.bottom
        anchors.topMargin: 10
        anchors.right: tickRow.right
    }

    Component.onCompleted: {
        var tickRelativeWidth = 1.0;
        var spaceRelativeWidth = 0.6;
        var tickRelativeHright = 2.5;
        var totalWidth = root.maxTicks * tickRelativeWidth + (root.maxTicks - 1) * spaceRelativeWidth;

        tickRow.tickWidth = Math.round(tickRelativeWidth * root.width / totalWidth);
        tickRow.tickHeight =Math.round(tickRow.tickWidth * tickRelativeHright);
        tickRow.spacing = Math.round(spaceRelativeWidth * root.width / totalWidth);

        maxTicksBar.height = tickRow.tickWidth;
        maxTicksBar.anchors.topMargin = tickRow.spacing;
        maxTicksBar.width = Math.round(root.maxTicks * tickRow.tickWidth + (root.maxTicks - 1) * tickRow.spacing);
    }
}
