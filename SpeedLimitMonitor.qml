import QtQuick 2.0

Item {
    id: root

    property int speedLimit

    width: childrenRect.width
    height: childrenRect.height

    Text {
        id: titleText

        text: "Speed limit"
        font.pointSize: 16
        color: "#4791FF"
        horizontalAlignment: Text.AlignHCenter
        anchors.left: parent.left
        anchors.right: parent.right
    }

    Item {
        id: secondRow

        width: childrenRect.width
        height: childrenRect.height
        anchors.top: titleText.bottom
        anchors.centerIn: parent.horizontalCenter

        Text {
            id: speedLimitText

            text: root.speedLimit
            font.pointSize: 30
            color: "#AFCFFF"
            anchors.left: parent.left
        }

        Text {
            id: unitsText

            text: "km/h"
            font.pointSize: 20
            color: "#4791FF"
            anchors.left: speedLimitText.right
            anchors.leftMargin: 10
            anchors.baseline: speedLimitText.baseline
        }
    }
}
